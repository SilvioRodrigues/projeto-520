import json

# PRO PYTHON ->  json são dicionários

with open("teste.json", "r") as file:
    reader = file.read()
    # print(reader)

arquivo = json.loads(reader)
# print(arquivo["glossary"]["GlossDiv"]["GlossList"]["GlossEntry"]["Acronym"])

# dicionario = {"nomes": {"Adultos": "Tiago, Caio, Kaique", "Crianças": "Maria, Joao"}}

# arquivo = json.dumps(dicionario)
# print(arquivo)

list_of_lists = [[1, 2, 3], [4, [2, 5, [12, 74, 2]], 6], [7, 8, 9]]
# for item in enumerate(list_of_lists[1]):
#     print(item)

# print(list_of_lists[1][1][2][1])

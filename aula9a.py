import os, csv

# Match e Case

x = "cpf"
y = "32165498755"

def func(teste, valor):
    match teste:

        case "cpf":
            cpf = valor.replace(".", "").replace("-", "").strip()
            if len(cpf) == 11 and cpf.isnumeric() == True:
                return True
            else:
                print("CPF inválido, digite novamente.")
                return False

        case "sexo":
            sexo = valor.upper().strip()
            if sexo in "MFO":
                return True
            else:
                print("Sexo inválido, digite novamente.")
                return False

        case "idade":
            idade = valor.strip()
            if idade.isnumeric() == True:
                if int(idade) < 130:
                    return True
            else:
                print("Idade inválida, digite novamente.")
                return False

        case _:
            pass


# Dunder name e dunder main:
if __name__ == "__main__":
    func(x, y)


# Abrindo multiplos arquivos com o OPEN:
def exclusao():
    cpf_nome = input("Digite o nome ou cpf da pessoa a excluir: ")

    with open("banco.csv", "r") as arquivo_inicial, open("banco_editado.csv", "a", newline="") as arquivo_editado:
        writer = csv.writer(arquivo_editado, delimiter=";")
        reader = csv.reader(arquivo_inicial, delimiter=";")

        for linha in reader:
            if linha[0] != cpf_nome and linha[1] != cpf_nome:
                writer.writerow(linha)
        
    os.remove("banco.csv")
    os.rename("banco_editado.csv", "banco.csv")

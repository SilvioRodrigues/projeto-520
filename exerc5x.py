import random, time

# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o pacote random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".

participantes = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", "Rosangela", "Rian"]
# INDEX             0       1        2         3          4         5        6        7           8          9

# n_jogadores = 9
# n_jogadores = len(participantes) -1
print("Começando os jogos!")
for rodada in range(1, 10):
    # eliminado = random.choice(participantes)

    # eliminado = participantes[random.randint(0, n_jogadores)]
    # n_jogadores -= 1

    eliminado = participantes[random.randint(0, len(participantes) -1)]
    # n_jogadores = len(participantes) -1
    
    print(f"Na {rodada}ª rodada, {eliminado} foi eliminado(a)")
    participantes.remove(eliminado)
    print("Começando próxima rodada...")
    time.sleep(2)

print(f"No fim, {participantes[0]} foi o vencedor(a)!")

# ===========================================================================================
# 2) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

# São 2587 ao todo.

def checaVogal(letra):
    if letra.lower() in "aeiouáéíóúãẽĩõũâêîôûàèìòùäëïöü":
        return True
    else:
        return False

vogais = 0
consoantes = set()

with open("faroeste.txt", "r", encoding="UTF-8") as arquivo:
    conteudo = arquivo.read()
    
    for letra in conteudo:
        if checaVogal(letra) == True:
            vogais += 1
        else:
            consoantes.add(letra)

print(f"Vogais: {vogais}")
print(f"Não vogais: \n{consoantes}")

# Tupla      = ()
# Lista      = []
# Dicionario = {chave:valor}
# Set        = {chave}


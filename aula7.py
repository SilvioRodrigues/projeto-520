# Tratamento de Exceções

try:                        # TENTE executar este código:
    print(int("abc"))

except ValueError:          # SE DER ERRO, execute este código:
    print("O programa deu uma exceção de ValueError")

else:                       # SE NAO DER ERRO, execute este codigo:
    print("Este trecho só é executado se o TRY for bem sucedido.")

finally:                    # SEMPRE execute este código:
    print("Este trecho de código sempre será executado no final.")


# Bancos de dados
# SQL -> Armazena informações em uma forma de Tabela.
import sqlite3

conexao = sqlite3.connect("empresa.db")  # Cria ou se conecta a um banco de dados existente.
cursor = conexao.cursor()                # Utilizado pra editar o banco.

# Criamos o comando pra gerar uma tabela:
cria_tabela = """
CREATE TABLE funcionarios (
id integer primary key autoincrement, 
nome text, 
idade integer
)"""

# Executamos um comando, salvamos e fechamos a conexão:
cursor.execute(cria_tabela)
conexao.commit()

# Criamos o comando pra adicionar um item em uma tabela:
adiciona_funcionario1 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Tiago", 27)"""

adiciona_funcionario2 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Kaique", 30)"""

adiciona_funcionario3 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Caio", 28)"""

cursor.execute(adiciona_funcionario1)
cursor.execute(adiciona_funcionario2)
cursor.execute(adiciona_funcionario3)
conexao.commit()

# Criamos o comando pra remover um item de uma tabela:
remove_funcionario = 'DELETE FROM funcionarios WHERE nome = "Tiago"'
cursor.execute(remove_funcionario)
conexao.commit()

# Criamos o comando pra editar um valor na tabela:
atualiza_valor = 'UPDATE funcionarios SET idade = 28 WHERE id = 2'
cursor.execute(atualiza_valor)
conexao.commit()

# Criamos o comando pra checar o conteudo da tabela:
checa_tabela = "SELECT * FROM funcionarios"
# checa_tabela = "SELECT idade FROM funcionarios"
# checa_tabela = "SELECT * FROM funcionarios WHERE nome = 'Tiago'"
cursor.execute(checa_tabela)
conteudo = cursor.fetchall()

for linha in conteudo:
    print(linha)

conexao.close()


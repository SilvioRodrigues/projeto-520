# ====================================================================================
# 1) Escreva um programa utilizando funções que realize um cadastro.
# Deverão ser coletadas as seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Cidade

# Os registros deverão ser armazenados em um arquivo CSV.
# Para manter o padrão brasileiro, o CSV será separado pelo caractere ";".
# O programa deverá possuir uma função de consulta e de exclusão (POR NOME OU CPF).
# O programa deverá possuir tratamentos de erro, com a finalidade de que o programa nunca
# dê uma exceção e também que ele não aceite dados incorretos em nenhum momento.

# 1: Importar os modulos a serem usados.
# 1a- Criar uma funcao para escolher a opcao
# 2: Criar uma função para coletar as informações do cliente.
# 3: Criar uma função para validar as informações passadas pelo cliente.
# 4: Criar uma função para adicionar essas informações em um arquivo CSV.
# 5: Criar uma função para consultar o arquivo CSV.
# 6: Criar uma função para remover entradas do arquivo CSV.

import csv       #importa csv - formato brasil com ; cmo separador


def cadastrar_registro():
    while True:
        try:
            cpf = input("Digite o CPF (somente números): ")
            if len(cpf) != 11 or not cpf.isdigit():
                raise ValueError("CPF inválido. O CPF deve conter 11 números.")
            
            nome = input("Digite o nome: ")
            idade = int(input("Digite a idade: "))
            sexo = input("Digite o sexo (M/F): ").upper()
            if sexo not in ['M', 'F']:
                raise ValueError("Sexo inválido. Digite M para masculino ou F para feminino.")
            
            cidade = input("Digite a cidade: ")

            with open("registros.csv", mode='a', newline='') as file:
                writer = csv.writer(file, delimiter=';')
                writer.writerow([cpf, nome, idade, sexo, cidade])
            
            print("Registro cadastrado com sucesso!")
            break
        except ValueError as ve:
            print(f"Erro: {ve}")

cadastrar_registro()

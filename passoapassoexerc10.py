# Utilizando todo o conteudo que aprendeu no treinamento, crie uma aplicação capaz de simular o 
# funcionamento de um caixa eletrônico. O caixa deverá possuir as seguintes funções:
# - Registrar novos clientes.
# - Fazer login dos clientes.
# - Checar o saldo dos clientes.
# - Realizar depósitos.
# - Realizar saques.

# Faça o uso de Classes, funções e tratamentos de erro para otimizar seu código.

# - Clientes podem ser criados a partir de uma classe.
# - Informações dos clientes, como nome, senha e saldo, podem ser armazenadas em um banco de dados.
# - Podemos criar funções para tomar as ações do caixa eletrônico.
# - Podemos fazer uso de diversos arquivos pra segmentar o nosso código.

# =================================================================================================
# 1° - Criar um ambiente virtual para o exercicio.
# 2° - Criar um arquivo pra lidar com o banco de dados.
# 3° - Criar as tabelas no DB, uma pra armazenar as informações do cliente (nome, idade, saldo)
# e outra pra armazenar os usuarios/senha dos clientes.
# 4° - Criar um arquivo pra armazenar a classes utilizada no programa.
# 5° - Criar um arquivo que conterá as funções executadas pelo programa (registrar, deposito, saque).
# 6° - No meu arquivo principal, eu criariaria um menu para executar o programa.
# 7° - Corrigir valores repetidos sendo adicionados na primeira execução.
# 8° - Fazer um tratamento de erros em todo o programa.
# 9° - Melhorar a qualidade de uso do programa para o cliente. (Melhorar a comunicação)

